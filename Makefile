#
# Main code
#

MAIN_SOURCES = \
	main.c \
	$(NULL)

CFLAGS = \
	-Wdeclaration-after-statement -Wextra -Wformat=2 -Winit-self -Winline -Wpacked -Wpointer-arith -Wlarger-than-65500 -Wmissing-declarations \
	-Wmissing-format-attribute -Wmissing-noreturn -Wmissing-prototypes -Wnested-externs -Wold-style-definition -Wsign-compare \
	-Wstrict-aliasing=2 -Wstrict-prototypes -Wswitch-enum -Wundef -Wunsafe-loop-optimizations -Wwrite-strings -Wno-missing-field-initializers \
	-Wno-unused-parameter -Wshadow -Wcast-align -Wformat-nonliteral -Wformat-security -Wswitch-default -Wmissing-include-dirs \
	-Waggregate-return -Wredundant-decls -Warray-bounds -Wunreachable-code
# CFLAGS which aren't supported on the lab machines
# CFLAGS += \
#	-Wunused-but-set-variable

main: $(MAIN_SOURCES:.c=.o)
	@gcc ${CPPFLAGS} -Wall ${CFLAGS} -o $@ $^
%.o: %.c
	@gcc ${CPPFLAGS} -Wall ${CFLAGS} -o $@ -c $^

all: main
.PHONY: all

clean-main:
	rm -f $(MAIN_SOURCES:.c=.o) main
.PHONY: clean-main


#
# Clean
#

clean: clean-main
.PHONY: clean
