/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

/* Command line exit statuses. */
enum {
	STATUS_SUCCESS = 0,
	STATUS_INVALID_COMMAND = 1,
	STATUS_INVALID_FILENAME = 2,
	STATUS_MISC_ERROR = 3,
	STATUS_INVALID_INPUT = 4,
	STATUS_IO_ERROR = 5,
	STATUS_UNSUPPORTED_VERSION = 6,
};

/* Standard brew results header. */
typedef uint8_t BrewConfigId;

typedef struct {
	uint8_t version;		/**< Brew results format version number. */
	BrewConfigId brew_id;		/**< ID of the brew configuration for these results. */
} __attribute__ ((packed)) SdCardBrewResultsHeader;

/* Version 1 of the brew results log entry data structure. */
typedef uint16_t TemperatureReadingVersion1;
typedef uint32_t TimerReadingVersion1;
typedef uint16_t HumidityReadingVersion1;
typedef uint8_t KrausenLevelReadingVersion1;
typedef uint16_t GasReadingVersion1;
typedef uint8_t BrewResultsChecksumVersion1;

typedef struct {
	HumidityReadingVersion1 relative_humidity;
	TemperatureReadingVersion1 temperature;
} __attribute__ ((packed)) HumidityTemperatureReadingVersion1;

#define NUM_THERMOMETERS_VERSION1 2

typedef struct {
	/* Metadata. */
	TimerReadingVersion1 time;							/**< Time at which the measurement was taken. */

	/* Sensor readings. */
	HumidityTemperatureReadingVersion1 humidity_reading;				/**< Relative humidity. */
	TemperatureReadingVersion1 temperature_reading[NUM_THERMOMETERS_VERSION1];	/**< Temperatures. */
	KrausenLevelReadingVersion1 krausen_level_reading;				/**< Krausen level. */
	GasReadingVersion1 gas_reading;							/**< Alcohol gas concentration. */

	/* Checksum. */
	BrewResultsChecksumVersion1 checksum;
} __attribute__ ((packed)) BrewResultsLogEntryVersion1;

/* Standard brew config header. */
typedef uint8_t BrewConfigChecksum;

typedef struct {
	uint8_t version;		/**< Brew configuration format version number. */
	BrewConfigChecksum checksum;	/**< Brew configuration data checksum over all fields in the BrewConfig. */
} __attribute__ ((packed)) SdCardBrewConfigHeader;

/* Version 1 of the brew config data structure. */
#define BREW_CONFIG_VERSION1_BREW_NAME_LENGTH 100 /* bytes */
#define BREW_CONFIG_VERSION1_START_DATE_LENGTH 17 /* bytes */

typedef struct {
	BrewConfigId brew_id; /**< Pseudo-random ID for this brew, where DEFAULT_BREW_ID is reserved for the default brew config. */

	char start_date[BREW_CONFIG_VERSION1_START_DATE_LENGTH]; /**< Start date/time for the brew in ISO 8601 format, e.g. "2012-11-27T03:31Z". */
	char brew_name[BREW_CONFIG_VERSION1_BREW_NAME_LENGTH]; /**< Human-readable name for the brew, e.g. "Geordie Bitter". */
	TemperatureReadingVersion1 fermentation_temperature; /**< Desired fermenter temperature in tenths of a degree Celsius, e.g. 215 for 21.5 degrees. */
} __attribute__ ((packed)) BrewConfigVersion1;

/* Calculate a checksum for the brew config. The Dallas/Maxim CRC-8 from avr-libc is used.
 * It uses polynomial: x^8 + x^5 + x^4 + 1 with initial value 0x00.
 * See: http://www.nongnu.org/avr-libc/user-manual/group__util__crc.html#ga37b2f691ebbd917e36e40b096f78d996 */
static uint8_t _crc_ibutton_update (uint8_t crc, uint8_t data)
{
	uint8_t i;

	crc = crc ^ data;
	for (i = 0; i < 8; i++) {
		if (crc & 0x01) {
			crc = (crc >> 1) ^ 0x8C;
		} else {
			crc >>= 1;
		}
	}

	return crc;
}

static BrewConfigChecksum _brew_config_calculate_checksum (const uint8_t *data, size_t n_bytes, uint8_t version)
{
	uint8_t crc = 0;
	size_t i;

	crc = _crc_ibutton_update (crc, version);
	for (i = 0; i < n_bytes; i++) {
		crc = _crc_ibutton_update (crc, data[i]);
	}

	return crc;
}

static int _create_config (const char *output_filename)
{
	time_t current_time;
	struct tm tmp;
	char start_date_with_nul[BREW_CONFIG_VERSION1_START_DATE_LENGTH + 1];
	char brew_name_with_nul[BREW_CONFIG_VERSION1_BREW_NAME_LENGTH + 1];
	char fermentation_temperature_with_nul[4 + 1]; /* e.g. ‘21.6’ plus trailing nul */
	float fermentation_temperature;
	char *fermentation_temperature_end_ptr = NULL;
	FILE *output_file = NULL;

	SdCardBrewConfigHeader config_header;
	union {
		BrewConfigVersion1 version1;
	} config;

	/* For the moment, just create version 1 brew config files. A --config-version command line parameter can be added later if necessary. */
	memset (&config, 0, sizeof (config));

	/* Generate a brew config ID. */
	current_time = time (NULL);
	config.version1.brew_id = current_time & 0xffff;

	/* Set the start date. */
	localtime_r (&current_time, &tmp);
	if (strftime (start_date_with_nul, sizeof (start_date_with_nul), "%FT%RZ", &tmp) != sizeof (start_date_with_nul) - 1 /* trailing nul */) {
		fputs ("Error writing date to brew configuration.\n", stderr);
		return STATUS_MISC_ERROR;
	}

	memcpy (config.version1.start_date, start_date_with_nul, sizeof (config.version1.start_date));

	/* Ask the user for a brew name. */
	puts ("Please enter a name for the brew:");
	if (fgets (brew_name_with_nul, sizeof (brew_name_with_nul), stdin) == NULL) {
		fputs ("Error reading brew name from command line.\n", stderr);
		return STATUS_INVALID_INPUT;
	}

	/* Remove the trailing new line from brew_name_with_nul. */
	if (brew_name_with_nul[strlen (brew_name_with_nul) - 1] == '\n') {
		brew_name_with_nul[strlen (brew_name_with_nul) - 1] = '\0';
	}

	strncpy (config.version1.brew_name, brew_name_with_nul, sizeof (config.version1.brew_name));

	/* Ask the user for a fermentation temperature. */
	puts ("Please enter the fermentation temperature in degrees Celsius (e.g. ‘21.4’):");
	if (fgets (fermentation_temperature_with_nul, sizeof (fermentation_temperature_with_nul), stdin) == NULL) {
		fputs ("Error reading fermentation temperature from command line.\n", stderr);
		return STATUS_INVALID_INPUT;
	}

	errno = 0;
	fermentation_temperature = strtof (fermentation_temperature_with_nul, &fermentation_temperature_end_ptr);

	if (errno != 0) {
		fprintf (stderr, "Error converting fermentation temperature ‘%s’ to a float.\n", fermentation_temperature_with_nul);
		return STATUS_INVALID_INPUT;
	}

	config.version1.fermentation_temperature = fermentation_temperature * 10; /* convert to tenths of a degree Celsius */

	/* Fill out the config header. */
	config_header.version = 1;
	config_header.checksum = _brew_config_calculate_checksum ((const uint8_t *) &(config.version1), sizeof (config.version1), 1);

	/* Write out to the file. */
	output_file = fopen (output_filename, "wb");

	if (output_file == NULL) {
		fprintf (stderr, "Error opening brew config file ‘%s’ for output.\n", output_filename);
		return STATUS_IO_ERROR;
	}

	if (fwrite (&config_header, sizeof (config_header), 1, output_file) != 1 ||
	    fwrite (&(config.version1), sizeof (config.version1), 1, output_file) != 1) {
		fprintf (stderr, "Error writing to brew config file ‘%s’.\n", output_filename);
		fclose (output_file);
		return STATUS_IO_ERROR;
	}

	fclose (output_file);

	return STATUS_SUCCESS;
}

static BrewResultsChecksumVersion1 _brew_results_calculate_checksum (const uint8_t *data, size_t n_bytes)
{
	uint8_t crc = 0;
	size_t i;

	crc = 0;
	for (i = 0; i < n_bytes; i++) {
		crc = _crc_ibutton_update (crc, data[i]);
	}

	return crc;
}

static int _show_results (const char *results_filename)
{
	FILE *results_file = NULL;
	unsigned int i;

	SdCardBrewResultsHeader results_header;
	union {
		BrewResultsLogEntryVersion1 version1;
	} log_entry;

	/* Open the file. */
	results_file = fopen (results_filename, "rb");

	if (results_file == NULL) {
		fprintf (stderr, "Error opening brew results file ‘%s’ for input.\n", results_filename);
		return STATUS_IO_ERROR;
	}

	/* Read the header. */
	if (fread (&results_header, sizeof (results_header), 1, results_file) != 1) {
		fprintf (stderr, "Error reading header from results file ‘%s’.\n", results_filename);
		fclose (results_file);
		return STATUS_IO_ERROR;
	}

	/* Check the header version. */
	if (results_header.version != 1) {
		fprintf (stderr, "Results file ‘%s’ uses file format version %u. Only version 1 is supported.\n", results_filename,
		         results_header.version);
		fclose (results_file);
		return STATUS_UNSUPPORTED_VERSION;
	}

	/* Output the brew ID. */
	printf ("Brew ID: %u\n\n", results_header.brew_id);

	/* Read and output the log entries. */
	for (i = 0; ; i++) {
		unsigned int j;
		BrewResultsChecksumVersion1 checksum;

		/* Read a single log entry and checksum. */
		if (fread (&(log_entry.version1), sizeof (log_entry.version1), 1, results_file) != 1) {
			if (feof (results_file) != 0) {
				/* Reached the end of the file. */
				break;
			}

			/* Error! */
			fprintf (stderr, "Error reading log entry %u from results file ‘%s’.\n", i + 1, results_filename);
			fclose (results_file);
			return STATUS_IO_ERROR;
		}

		/* Check its checksum. TODO: This is hacky. */
		checksum = _brew_results_calculate_checksum ((const uint8_t *) &(log_entry.version1),
		                                             sizeof (log_entry.version1) - sizeof (BrewResultsChecksumVersion1));

		/* Print it out. */
		if (checksum == log_entry.version1.checksum) {
			printf ("Entry %u:\n", i + 1);
		} else {
			printf ("Entry %u (INVALID CHECKSUM: expected %u but got %u):\n", i + 1, log_entry.version1.checksum, checksum);
		}

		printf ("   Time:                       %u\n", log_entry.version1.time);
		printf ("   Humidity:                   %u.%u%% RH at %u.%u℃\n",
		        log_entry.version1.humidity_reading.relative_humidity / 10,
		        log_entry.version1.humidity_reading.relative_humidity % 10,
		        log_entry.version1.humidity_reading.temperature / 10,
		        log_entry.version1.humidity_reading.temperature % 10);
		for (j = 0; j < NUM_THERMOMETERS_VERSION1; j++) {
			printf ("   Temperature %u:              %u.%u℃\n", j + 1,
			        log_entry.version1.temperature_reading[j] / 10, log_entry.version1.temperature_reading[j] % 10);
		}
		printf ("   Krausen level:              %u.%u cm\n",
		        log_entry.version1.krausen_level_reading / 10, log_entry.version1.krausen_level_reading % 10);
		printf ("   Alcohol gas concentration:  %u.%02u mg⋅l⁻¹\n",
		        log_entry.version1.gas_reading / 100, log_entry.version1.gas_reading % 100);
		puts (""); /* empty line */
	}

	fclose (results_file);

	return STATUS_SUCCESS;
}

static void _usage (const char *program_name)
{
	printf ("Usage:\n  %s COMMAND FILE\n\n", program_name);
	puts ("Commands:");
	puts ("  create-config        Create a brew configuration");
	puts ("  show-results         Display brew results");
}

int main (int argc, char *argv[])
{
	const char *command, *filename;

	/* Argument handling. */
	if (argc < 2) {
		fputs ("Error: Missing command.\n\n", stderr);
		_usage (argv[0]);
		return STATUS_INVALID_COMMAND;
	} else if (argc < 3) {
		fputs ("Error: Missing filename.\n\n", stderr);
		_usage (argv[0]);
		return STATUS_INVALID_FILENAME;
	}

	command = argv[1];
	filename = argv[2];

	if (strcmp (command, "create-config") == 0) {
		return _create_config (filename);
	} else if (strcmp (command, "show-results") == 0) {
		return _show_results (filename);
	} else {
		/* Invalid command. */
		fprintf (stderr, "Error: Invalid command: ‘%s’.\n\n", command);
		_usage (argv[0]);
		return STATUS_INVALID_COMMAND;
	}
}
